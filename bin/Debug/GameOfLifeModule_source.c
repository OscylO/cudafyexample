#if defined(cl_khr_fp64)
#pragma OPENCL EXTENSION cl_khr_fp64: enable
#elif defined(cl_amd_fp64)
#pragma OPENCL EXTENSION cl_amd_fp64: enable
#endif

// CudafyExample.GameOfLifeModule
__kernel  void Launch(global unsigned char* inputData, int inputDataLen0, int inputDataLen1, int width, int height, global unsigned char* resultData, int resultDataLen0, int resultDataLen1);
// CudafyExample.GameOfLifeModule
  unsigned char CountAliveCells(int x, int y, local unsigned char* data, int dataLen0, int dataLen1);

// CudafyExample.GameOfLifeModule
__kernel  void Launch(global unsigned char* inputData, int inputDataLen0, int inputDataLen1, int width, int height, global unsigned char* resultData, int resultDataLen0, int resultDataLen1)
{
	__local unsigned char array[18*18];

	int arrayLen0 = 18;
	int arrayLen1 = 18;
	int num = get_group_id(0) * get_local_size(0) + get_local_id(0);
	int num2 = get_group_id(1) * get_local_size(1) + get_local_id(1);
	if (num < width && num2 < height)
	{
		array[(0) * arrayLen1 + ( get_local_id(1) + 1)] = ((get_local_id(0) == 0 && num > 0) ? inputData[(num - 1) * inputDataLen1 + ( num2)] : 0);
		array[(get_local_id(0) + 1) * arrayLen1 + (0)] = ((get_local_id(1) == 0 && num2 > 0) ? inputData[(num) * inputDataLen1 + ( num2 - 1)] : 0);
		if (get_local_id(1) == get_local_size(1) && num2 < width)
		{
			array[(get_local_id(0) + 1) * arrayLen1 + ( get_local_size(1) + 2)] = inputData[(num) * inputDataLen1 + ( num2 + 1)];
		}
		if (get_local_id(0) == get_local_size(0) && num < height)
		{
			array[(get_local_id(0) + 2) * arrayLen1 + ( get_local_id(1) + 1)] = inputData[(num + 1) * inputDataLen1 + ( num2)];
		}
		unsigned char b = array[(get_local_id(0) + 1) * arrayLen1 + ( get_local_id(1) + 1)] = inputData[(num) * inputDataLen1 + ( num2)];
		barrier(CLK_LOCAL_MEM_FENCE);
		unsigned char b2 = CountAliveCells(get_local_id(0) + 1, get_local_id(1) + 1, array, arrayLen0, arrayLen1);
		resultData[(num) * resultDataLen1 + ( num2)] = ((b2 == 3 || (b2 == 2 && b != 0)) ? 1 : 0);
	}
}
// CudafyExample.GameOfLifeModule
  unsigned char CountAliveCells(int x, int y, local unsigned char* data, int dataLen0, int dataLen1)
{
	return data[(x - 1) * dataLen1 + ( y - 1)] + data[(x) * dataLen1 + ( y - 1)] + data[(x + 1) * dataLen1 + ( y - 1)] + data[(x - 1) * dataLen1 + ( y)] + data[(x + 1) * dataLen1 + ( y)] + data[(x - 1) * dataLen1 + ( y + 1)] + data[(x) * dataLen1 + ( y + 1)] + data[(x + 1) * dataLen1 + ( y + 1)];
}
