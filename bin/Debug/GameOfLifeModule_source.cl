#if defined(cl_khr_fp64)
#pragma OPENCL EXTENSION cl_khr_fp64: enable
#elif defined(cl_amd_fp64)
#pragma OPENCL EXTENSION cl_amd_fp64: enable
#endif

// CudafyExample.GameOfLifeModule
__kernel  void Launch(global unsigned char* inputData, int inputDataLen0, int inputDataLen1, int width, int height, global unsigned char* resultData, int resultDataLen0, int resultDataLen1);
// CudafyExample.GameOfLifeModule
  unsigned char CountAliveCells(int x, int y, local unsigned char* data, int dataLen0, int dataLen1);

// CudafyExample.GameOfLifeModule
__kernel  void Launch(global unsigned char* inputData, int inputDataLen0, int inputDataLen1, int width, int height, global unsigned char* resultData, int resultDataLen0, int resultDataLen1)
{
	__local unsigned char array[16*16];

	int arrayLen0 = 16;
	int arrayLen1 = 16;
	int num = get_group_id(0) * get_local_size(0) + get_local_id(0) - get_group_id(0) * 2 - 1;
	int num2 = get_group_id(1) * get_local_size(1) + get_local_id(1) - get_group_id(1) * 2 - 1;
	int x = get_local_id(0);
	int y = get_local_id(1);
	bool flag = false;
	unsigned char b = 0;
	if (num >= 0 && num2 >= 0 && num < width && num2 < height)
	{
		b = inputData[(num) * inputDataLen1 + ( num2)];
		flag = true;
	}
	array[(x) * arrayLen1 + ( y)] = b;
	barrier(CLK_LOCAL_MEM_FENCE);
	if (flag && x > 0 && y > 0 && x < 15 && y < 15)
	{
		unsigned char b2 = CountAliveCells(x, y, array, arrayLen0, arrayLen1);
		resultData[(num) * resultDataLen1 + ( num2)] = ((b2 == 3 || (b2 == 2 && b != 0)) ? 1 : 0);
	}
}
// CudafyExample.GameOfLifeModule
  unsigned char CountAliveCells(int x, int y, local unsigned char* data, int dataLen0, int dataLen1)
{
	return data[(x - 1) * dataLen1 + ( y - 1)] + data[(x) * dataLen1 + ( y - 1)] + data[(x + 1) * dataLen1 + ( y - 1)] + data[(x - 1) * dataLen1 + ( y)] + data[(x + 1) * dataLen1 + ( y)] + data[(x - 1) * dataLen1 + ( y + 1)] + data[(x) * dataLen1 + ( y + 1)] + data[(x + 1) * dataLen1 + ( y + 1)];
}
