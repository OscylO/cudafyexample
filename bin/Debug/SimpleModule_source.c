#if defined(cl_khr_fp64)
#pragma OPENCL EXTENSION cl_khr_fp64: enable
#elif defined(cl_amd_fp64)
#pragma OPENCL EXTENSION cl_amd_fp64: enable
#endif

// CudafyExample.SimpleModule
__kernel  void Launch(global int* array, int arrayLen0, int arrayLen1, int width, int height);

// CudafyExample.SimpleModule
__kernel  void Launch(global int* array, int arrayLen0, int arrayLen1, int width, int height)
{
	int num = get_group_id(0) * get_local_size(0) + get_local_id(0);
	int num2 = get_group_id(1) * get_local_size(1) + get_local_id(1);
	if (num < width && num2 < height)
	{
		array[(num) * arrayLen1 + ( num2)] *= array[(num) * arrayLen1 + ( num2)];
	}
}
