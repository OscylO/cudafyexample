﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Drawing;

using Cudafy;
using Cudafy.Host;
using Cudafy.Translator;
using System.Runtime.InteropServices;

namespace CudafyExample
{
    class Program
    {
        [DllImport("user32.dll", EntryPoint = "SetWindowPos")]
        public static extern IntPtr SetWindowPos(IntPtr hWnd, int hWndInsertAfter, int x, int Y, int cx, int cy, int wFlags);

        private const string ModuleName = "GameOfLifeModule";
        private static readonly dim3 BlockSize = new dim3(16, 16); // Controls maximum number of threads.
        private static GPGPU _gpgpu; // GPGPU device used to for calculations.
        private static CudafyModule _module;
        private byte[,] _devInputData;  // Pointer to GPGPU data.
        private byte[,] _devOutputData; // Pointer to GPGPU data.
        
        private static void Main(string[] args)
        {
            new Program();
        }

        private Program()
        {
            try
            {
                ConfigureConsoleWindow();

         
                _gpgpu = GetGPGPUDevice(eGPUType.OpenCL);

                if (_gpgpu == null)
                {
                    Console.WriteLine("No suitable {0} devices found.", CudafyModes.Target);
                    return;
                }

                _module = CudafyModule.TryDeserialize(ModuleName);
                if (_module == null || !_module.TryVerifyChecksums())
                {
                    _module = CudafyTranslator.Cudafy(eArchitecture.OpenCL, typeof(GameOfLifeModule));
                    _module.Name = ModuleName;
                    _module.Serialize(ModuleName);
                }

                try
                {
                    _gpgpu.LoadModule(_module);
                }
                catch
                {
                    Console.WriteLine("Error: Filed to load Game Of Life module onto GPGPU.");
                }
                finally
                {
                    // Creates file containing source code.
                    File.WriteAllText(ModuleName + "_source.cl", _module.SourceCode);
                }

                // Dimensions of board where cells live.
                Size boardSize = new Size(Console.WindowWidth - 2, Console.WindowHeight - 5);

                StartGameOfLife(boardSize, 500);
            }
            finally
            {
                if (_gpgpu != null)
                {
                    // Removes all data allocated on GPGPU by this application.
                    _gpgpu.FreeAll();
                    _gpgpu.UnloadModules();
                }

                Console.WriteLine("Press any key to close.");
                Console.ReadKey();
            }
        }

        private static void ConfigureConsoleWindow()
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Title = "CudafyExample by Oskar Wicha - Game Of Life";
            Console.SetWindowSize(Console.LargestWindowWidth - 5, Console.LargestWindowHeight - 5);
            SetWindowPos(Process.GetCurrentProcess().MainWindowHandle, 0, 0, 0, 0, 0, 0x0001);
        }

        private void PrintArrayContents(Size boardSize, byte[,] results)
        {
            var stringBuilder = new StringBuilder();

            for (int y = 0; y < boardSize.Height; y++)
            {
                for (int x = 0; x < boardSize.Width; x++)
                {
                    stringBuilder.Append(results[x, y] == 0 ? ' ' : '0');
                }
                    
                stringBuilder.AppendLine();
            }
                
            Console.Write(stringBuilder.ToString());
        }

        private void GenerateInputData(byte[,] data)
        {
            var randomGen = new Random();
            for (int x = 0; x < data.GetLength(0); x++)
            {
                for (int y = 0; y < data.GetLength(1); y++)
                {
                    data[x, y] = (byte)randomGen.Next(2);
                }
            }
        }

        private GPGPU GetGPGPUDevice(eGPUType type)
        {
            CudafyModes.Target = type;
            CudafyTranslator.Language = (CudafyModes.Target == eGPUType.OpenCL)
                ? eLanguage.OpenCL
                : eLanguage.Cuda;
            GPGPUProperties[] compatibleDevices =
                CudafyHost.GetDeviceProperties(CudafyModes.Target, true).ToArray();

            if (!compatibleDevices.Any())
            {
                Console.WriteLine(
                    "Error: No devices supporting OpenCL. Check if you have latest drivers installed.");
                return null;
            }

            foreach (var device in compatibleDevices)
            {
                Console.WriteLine(
                    "Device id : {0} \r\nName: '{2}'\r\nPlatform: '{1}'\r\n",
                    device.DeviceId,
                    (device.PlatformName ?? string.Empty).Trim(),
                    device.Name.Trim());
            }

            Console.WriteLine("\r\nSelect device with id: ");

            int selectedId;

            do
            {
                selectedId = Console.ReadKey(true).KeyChar - '0';
            } while (compatibleDevices.All(device => device.DeviceId != selectedId));


            GPGPUProperties selectedDevice = compatibleDevices[selectedId];

            if (selectedDevice == null)
                return null;

            CudafyModes.DeviceId = selectedDevice.DeviceId;
            Console.WriteLine(
                "\r\nSelected device id : {0} \r\nName: '{2}'\r\nPlatform: '{1}'\r\n",
                selectedDevice.DeviceId,
                (selectedDevice.PlatformName ?? string.Empty).Trim(),
                selectedDevice.Name.Trim());

            return CudafyHost.GetDevice(CudafyModes.Target, CudafyModes.DeviceId);
        }

        private void StartGameOfLife(Size boardSize, int iterationsCount)
        {
            var cursorPositionTop = Console.CursorTop;
            var inputData = new byte[boardSize.Width, boardSize.Height];
            var tempBuffer = new byte[boardSize.Width, boardSize.Height];
            var gridSize = new dim3(
                (int)Math.Ceiling(boardSize.Width  / (double)(BlockSize.x - 2)),
                (int)Math.Ceiling(boardSize.Height / (double)(BlockSize.y - 2)));

            if (!_gpgpu.IsModuleLoaded(ModuleName))
            {
                _module.SuppressWindow = true;
                _gpgpu.LoadModule(_module);
            }

            AllocateMemoryOnGPGPU(boardSize);
            GenerateInputData(inputData);
            _gpgpu.CopyToDevice(inputData, _devInputData);

            if (!_gpgpu.IsOnGPU(_devInputData))
            {
                Console.WriteLine("Failed to copy data to GPGPU.");
                return;
            }

            ExecuteBenchmark(iterationsCount, gridSize, BlockSize, boardSize);

            if (boardSize.Width > Console.WindowWidth || boardSize.Height > Console.WindowHeight)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Data size is to big to show iterations.");
                return;
            }

            Console.WriteLine("Press any key to show all iterations.");
            Console.ReadKey();
           
            // Show all iterations.
            _gpgpu.CopyToDevice(inputData, _devInputData);
            for (int i = 0; i < iterationsCount; i++)
            {
                LaunchGameOfLifeModule(gridSize, BlockSize, boardSize);

                _gpgpu.CopyFromDevice(_devOutputData, tempBuffer);
                SwapPointers(ref _devInputData, ref _devOutputData);

                ShowGameState(cursorPositionTop, boardSize, tempBuffer);
                Console.WriteLine("Iteration {0}/{1}", i + 1, iterationsCount);
                Thread.Sleep(150);
            }
        }

        private void AllocateMemoryOnGPGPU(Size boardSize)
        {
            _devInputData = _gpgpu.Allocate<byte>(boardSize.Width, boardSize.Height);
            _devOutputData = _gpgpu.Allocate<byte>(boardSize.Width, boardSize.Height);
        }

        private void ExecuteBenchmark(int iterationsCount, dim3 gridSize, dim3 blockSize, Size boardSize)
        {
            Console.WriteLine("Wait...");
            var sw = Stopwatch.StartNew();

            for (int i = 0; i < iterationsCount; i++)
            {
                LaunchGameOfLifeModule(gridSize, blockSize, boardSize);
                SwapPointers(ref _devInputData, ref _devOutputData);
            }

            var executionTime = sw.Elapsed.TotalMilliseconds;
            Console.WriteLine(
                "Executed {1} iterations of {2}x{3} Game of Life in {0} ms",
                executionTime,
                iterationsCount,
                boardSize.Width,
                boardSize.Height);
        }

        private void ShowGameState(int cursorPositionTop, Size boardSize, byte[,] tempBuffer)
        {
            Console.SetCursorPosition(0, cursorPositionTop);
            Console.WriteLine("Results:");
            PrintArrayContents(boardSize, tempBuffer);
        }

        private void LaunchGameOfLifeModule(dim3 gridSize, dim3 blockSize, Size boardSize)
        {
            _gpgpu.Launch(
                gridSize,
                blockSize,
                GameOfLifeModule.Launch,
                _devInputData,
                boardSize.Width,
                boardSize.Height,
                _devOutputData);
        }

        /// <summary>
        /// After execution of pointer1 will point where pointer 2 pointed
        /// before function invocation and pointer 2 will point where
        /// pointer1 previously pointed.
        /// </summary>
        /// <typeparam name="T">Data type of pointers</typeparam>
        /// <param name="pointer1">GPU memory pointer</param>
        /// <param name="pointer2">GPU memory pointer</param>
        private void SwapPointers<T>(ref T pointer1, ref T pointer2)
        {
            var temp = pointer1;
            pointer1 = pointer2;
            pointer2 = temp;
        }
    }
}
