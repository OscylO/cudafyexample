﻿using Cudafy;

namespace CudafyExample
{
    public static class GameOfLifeModule
    {
        [Cudafy(eCudafyType.Global)]
        public static void Launch(GThread thread,
                                  [CudafyAddressSpace(eCudafyAddressSpace.Global)]  byte[,] inputData,
                                  [CudafyAddressSpace(eCudafyAddressSpace.Private)] int width,
                                  [CudafyAddressSpace(eCudafyAddressSpace.Private)] int height,
                                  [CudafyAddressSpace(eCudafyAddressSpace.Global)]  byte[,] resultData)
        {
            byte[,] cache = thread.AllocateShared<byte>("cache", 16, 16);

            // Calculate position in array.
            int x = thread.blockIdx.x * thread.blockDim.x + thread.threadIdx.x - (thread.blockIdx.x * 2) - 1;
            int y = thread.blockIdx.y * thread.blockDim.y + thread.threadIdx.y - (thread.blockIdx.y * 2) - 1;
            int cellInCacheX = thread.threadIdx.x;
            int cellInCacheY = thread.threadIdx.y;
            bool isBoardCell = false;
            byte cellValue = 0;

            if (x >= 0 && y >= 0 && x < width && y < height)
            {
                cellValue = inputData[x, y];
                isBoardCell = true;
            }
            // Fill cache cells that will be processed by threads in this block.
            cache[cellInCacheX, cellInCacheY] = cellValue;
            // Wait until all threads in block finish loading data to shared array.
            thread.SyncThreads();

            if (isBoardCell && cellInCacheX > 0 && cellInCacheX < 15 && cellInCacheY > 0 && cellInCacheY < 15)
            {
                byte aliveCells = CountAliveCells(cellInCacheX, cellInCacheY, cache);
                resultData[x, y] = (byte)(aliveCells == 3 || (aliveCells == 2 && cellValue != 0) ? 1 : 0);
            }
        }

        [CudafyAttribute(eCudafyType.Device)]   // Indicates to compiler that it should convert it to OpenCL or CUDA function.
        [CudafyInline(eCudafyInlineMode.Force)] // Forces function inlinning.
        public static byte CountAliveCells(int x, int y, [CudafyAddressSpace(eCudafyAddressSpace.Shared)] byte[,] data)
        {
            return (byte)(data[x-1, y-1] + data[x, y-1] + data[x+1, y-1] +
                          data[x-1, y  ]                + data[x+1, y  ] +
                          data[x-1, y+1] + data[x, y+1] + data[x+1, y+1]);
        }
    }
}
